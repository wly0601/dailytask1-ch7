import Card from './components/card/Card';
import Header from './components/header/Header';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

function App() {
    return ( 
      <div className = "App" >
        < Header title = "FSW"
          greetings = "Hallo, Selamat Datang di Kelas"
          number = "13"
          // back = "grey"
        />

        < Card title = "Hello"
          description = "Lorem ipsum dolor sit amet"
          btnText = "Go Somewhere"
          imgSrc = "https://placeimg.com/320/240/any"
          imgAlt = "Hello"
          number = "9" 
        />
      </div>
    );
}

export default App;