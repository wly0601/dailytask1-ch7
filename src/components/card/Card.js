import React from "react";
import Header from '../header/Header'
import Button from "../button/Button";

const Card = (props) => {
  const { title, description, imgSrc, imgAlt, btnText, number} = props;
  const elements = [];

  for(let i = 0; i < parseInt(number); i++){
    elements.push( 
      <div class="col-sm my-4">
        <div className="card" style={{ width: "18rem" }}>
          <Header 
            title="Card"
            number={i+1}
          />
          <img src={imgSrc} className="card-img-top" alt={imgAlt} />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{description}</p>
          <Button variant="black">{btnText}</Button>
        </div>
      </div>
    </div>
    )
  }


  return (
    <div class="container">
      <div class="row">
        {elements}
      </div>
    </div>
  )
}

export default Card;
