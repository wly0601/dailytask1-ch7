import React from "react";

const Header = (props) => {
  const { title, number, greetings, back} = props;
  
  return (
    <div className="header" style={{ width: "100%"}}>
      <div className="card-body">
        <h1 className="card-title">{greetings} {title} {number}</h1>
      </div>
    </div>
  )
}

export default Header;
